<?php
/**
 * Bumper
 *
 * PHP Version: 7.x
 *
 * @category CLI
 * @package  Cove\Bumper
 * @author   Eivin Landa <landa@cove.no>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GNU GPLv3
 * @link     https://cove.no Deployment tool
 */

namespace Cove\Bumper;

/**
 * Bumper
 *
 * @category CLI
 * @package  Cove\Bumper
 * @author   Eivin Landa <landa@cove.no>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GNU GPLv3
 * @link     https://cove.no Deployment tool
 */
class Bumper
{
    // Build wonderful things
}
