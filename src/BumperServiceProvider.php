<?php

namespace Cove\Bumper;

use Cove\Bumper\Commands\Bump;
use Illuminate\Support\ServiceProvider;

class BumperServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
            $this->commands([Bump::class]);
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/bumper.php', 'bumper');
        // $this->app->bind(
        //     'Bumper', '\Cove\Bumper\Bumper'
        // );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['bumper'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/bumper.php' => config_path('bumper.php'),
        ], 'bumper.config');
    }
}
