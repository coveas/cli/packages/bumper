<?php

namespace Cove\Bumper\Commands;

use Illuminate\Console\Command;

class Bump extends Command
{
    public $signature = 'bump {part?} {--n|no-interaction} {--c|commit-only} {--m|message=} {--d|dry-run}';
    public $description = 'Bump the version number';
    public $dry = false;

    /**
     * Argument or choice
     *
     * @param string $key      Argument name
     * @param string $question Question
     * @param array  $choices  Choices
     *
     * @return string
     */
    public function argOrChoice($key, $question, $choices)
    {
        $value = $this->argument($key);
        if (! $value) {
            return (string) $this->choice($question, $choices);
        }
        return $value;
    }
    /**
     * One or choice
     * Get the first item from a collection if there is only one,
     * or ask the user to choose which item to use.
     *
     * @param Collection $collection Collection
     * @param string     $question   Question
     * @param Collection $choices    Choices
     *
     * @return mixed                  Chosen item
     */
    public function oneOrChoice($collection, $question, $choices)
    {
        if (1 == $collection->count()) {
            return $collection->keys()->first();
        }
        return $this->choice($question, $choices->toArray());
    }

    /**
     * Handle
     *
     * @return null
     */
    public function handle()
    {
        $interaction = ! $this->option('no-interaction');
        $message     = $this->option('message');
        $commitOnly  = $this->option('commit-only');
        $this->dry   = $this->option('dry-run');
        if ($this->dry) {
            $this->warn('This is a dry run!');
        }
        if (! $message || $message === true) {
            $message = 'Bumped the version number';
        }
        exec('git status -s 2>&1', $output, $result);
        if ($result) {
            $this->error('Git error. Is this a git repo?');
            return;
        }
        if (! empty($output)) {
            $this->error('There are unchanged files. Clean up first.');
            return;
        }
        // $bumper = app('Bumper', ['app' => $app]);
        // Get update information.
        exec('git remote update 2>&1', $output, $result);

        // @TODO: $bumper->check();
        // Check if we're ahead or behind
        exec('git status -uno 2>&1', $output, $result);

        if ($result || empty($output)) {
            $this->error('Could not get git status');
            return;
        }
        $output = implode("\n", $output);
        if (! preg_match('/Your branch is ((?:up to date)|\S+)/', $output, $matches)) {
            $this->error('Missing information about remote status from git status!');
            $this->info($output);
            return;
        }

        if (!preg_match('/up.to.date/', $matches['1'])) {
            $this->error("Your local repo is {$matches[1]}!");
            $this->warn("Please make sure you've pushed/pulled before bumping the version.");
            return;
        }

        // @TODO: $bumper->getCurrentVersion();
        // Get tags
        exec('git tag 2>&1', $versions, $result);
        array_filter($versions);
        if (empty($versions)) {
            $versions = ['0.0.0'];
        }
        usort($versions, 'version_compare');
        // Get the latest version number
        $latest = end($versions);

        // @TODO: $bumper->bump($part);
        $part = $this->argOrChoice(
            'part',
            'Which part should be bumped',
            [
                'extra' => 'Extra',
                'patch' => 'Patch',
                'minor' => 'Minor',
                'major' => 'Major',
            ]
        );
        if (! $part && ! $interaction) {
            $this->error('Part parameter is (patch, minor or major) is required when using no-interaction.');
            return;
        }

        $parts = explode('.', $latest);
        $count = count($parts);
        if ($count < 3) {
            throw new \Exception(
                "Expected version number, $latest, to have three parts (##.##.##), ".
                "but it had $count",
                1
            );
        }

        if ($part == 'major') {
            $parts[0] +=1;
            $parts[1] = 0;
            $parts[2] = 0;
        } else if ($part == 'minor') {
            $parts[1] += 1;
            $parts[2] = 0;
        } else if ($part == 'patch') {
            $parts[2] += 1;
        } else if ($part == 'extra') {
            $parts[3] += 1;
        }
        $newVersion = implode('.', $parts);
        if ($interaction && ! $this->confirm('Next version will be: '. $newVersion)) {
            return;
        }

        $editedFiles = [];

        $files = [
            'composer.json' => [
                '/\n(\s*"version"\s*:\s*")\d+\.\d+(?:\.\d+)+(",?)\s*?\n/',
                "\n\${1}{$newVersion}\${2}\n",
            ],
            'readme.txt' => [
                '/\n(\s*[Ss]table [Tt]ag\s*:\s*)\d+\.\d+(?:\.\d+)+\s*?\n/',
                "\n\${1}{$newVersion}\n",
            ],
            'plugin.php' => [
                '/\n(\s*\*?\s*[Vv]ersion\s*:\s*)\d+\.\d+(?:\.\d+)+\s*?\n/',
                "\n\${1}{$newVersion}\n",
            ],
            'style.css' => [
                '/\n(\s*\*?\s*[Vv]ersion\s*:\s*)\d+\.\d+(?:\.\d+)+\s*?\n/',
                "\n\${1}{$newVersion}\n",
            ],
        ];

        $foldername = basename(getcwd());
        $files["{$foldername}.php"] = $files['plugin.php'];

        foreach ($files as $filename => $regex) {
            $replaced = $this->replace($filename, $regex[0], $regex[1]);
            if ($replaced) {
                $editedFiles[] = $filename;
            }
        }

        $phpFiles = $this->regSearch('/.*\.php$/');
        foreach ($phpFiles as $file) {
            $filename = $file->getPathname();
            if (substr_count($filename, '/') > 2) {
                continue;
            }
            $replaced = $this->replace(
                $filename,
                '/\n(\s+const VERSION = )\'(\d\.[\d\.]+)\';/',
                "\n\${1}'{$newVersion}';"
            );
            if ($replaced) {
                $editedFiles[] = preg_replace('/^\.\\//', '', $filename);
            }
        }

        if (empty($editedFiles)) {
            $this->error('No files changed.');
        } else {
            $this->info(
                sprintf(
                    'Changed %d files',
                    count($editedFiles)
                )
            );
            foreach ($editedFiles as $file) {
                $this->info('* ' . $file);
            }
        }

        $question = 'Commit'. ( $commitOnly ? '' : ' and push') .' with the message, "'. $message .'"?';
        if ($interaction && ! $this->confirm($question)) {
            $this->warn("Ok, you're on your own.");
            return;
        }
        $command = sprintf(
            'git add '. implode(' ', $editedFiles). "\n".
            'git commit -m %s && git tag %s'.
            ($commitOnly ? '' : ' && git push 2>&1 && git push --tags 2>&1'),
            escapeshellarg($message),
            escapeshellarg($newVersion)
        );
        if ($this->dry) {
            $this->warn('Would run the following:');
            $this->warn($command);
        } else {
            exec($command);
            $this->info('Done. Committed and pushed version change.');
        }
    }

    /**
     * RegEx search for files
     *
     * @param string $pattern Regular expression.
     *
     * @return Collection Collection of files.
     */
    public function regSearch($pattern)
    {
        $it = new \RecursiveDirectoryIterator('.');
        $collection = collect();
        foreach (new \RecursiveIteratorIterator($it) as $file) {
            if (preg_match($pattern, $file)) {
                $collection->add($file);
            }
        }
        return $collection;
    }

    /**
     * Replace
     *
     * @param string $file    Filename.
     * @param string $regex   Regular expression.
     * @param string $replace Replace string.
     *
     * @return boolean True if the file was changed.
     */
    public function replace($file, $regex, $replace)
    {
        if (! file_exists($file)) {
            return false;
        }
        $content = file_get_contents($file);
        if (! preg_match($regex, $content)) {
            return false;
        }
        if ($this->dry) {
            $this->warn("Would edit $file.");
            return true;
        }
        $content = preg_replace($regex, $replace, $content);
        file_put_contents($file, $content);
        return true;
    }
}
